terraform {
  backend "s3" {
    bucket         = "test-2020-12-27"
    key            = "test-app.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "cqian-terraform-2020"
  }
}

provider "aws" {
  region  = "us-west-2"
  version = "~> 2.54.0"
  /*
   assume_role {
      role_arn     = "arn:aws:iam::546528649988:role/main-admin-role" 
      session_name = "Deploy_SESSION_NAME"
      external_id  = "EXTERNAL_ID"
   } 
*/
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    MangeBy     = "Terraform"
  }
}
